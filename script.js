const students = [];
function addStudent(name){
	students.push(name)
  console.log( `${name} was added to the list`)
}

addStudent("John")
addStudent("Jacob")
addStudent("Jingleheimer")
addStudent("Schmidt")

function countStudent(number){
  number = students.length;
  console.log(`There are a total of ${number} students enrolled.`)
}
countStudent()

function printStudent(){
  students.forEach(list => console.log(list))
}
printStudent()



function findStudent(name){
  const lname = name.toLowerCase()
	const foundStudents = students.filter(student => student.toLowerCase().includes(lname))
  const numResult = foundStudents.length
  
  if(numResult <= 0){
  	console.log(`${name} is not an enrollee`)
    return
  } else if (numResult === 1) {
    console.log(`${foundStudents[0]} is an enrollee`)
    return
  } else {
  	console.log(`${foundStudents.map(student => student).join(", ")} are enrollees`)
    return 
  }
}



